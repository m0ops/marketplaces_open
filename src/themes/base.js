import { createTheme } from "@material-ui/core/styles";


export const theme = createTheme({
  palette: {
    primary: {
      main: "#FF774D",
    },
    secondary: {
      main: "#11cb5f",
    },
  },
  typography: {
    fontFamily: 'Inter'
  }
});