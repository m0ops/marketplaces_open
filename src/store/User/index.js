import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isAuth: false,
  isBlock: false,
  userData: {
    email_is_check: "",
    email: "",
    id: null,
    first_name: "",
    last_name: "",
    username: "",
    phone_number: "",
  },
};

const userSlice = createSlice({
  name: "user",
  initialState: initialState,
  reducers: {
    setToken(action) {
      sessionStorage.setItem("token", action.payload);
      localStorage.setItem("token", action.payload);
    },
    setLastLogin(state, action) {
      state.last_login = action.payload;
    },
    setUserData(state, action) {
      state.userData = action.payload;
    },
  },
});

export const userActions = userSlice.actions;
export default userSlice.reducer;
