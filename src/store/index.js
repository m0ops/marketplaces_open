import { configureStore, combineReducers } from "@reduxjs/toolkit";

import userSlice from "./User";
// import { selectedApiSlice, apiSlice } from "./Api";
// import { selectedApiSlice, apiSlice } from "./Api";
import errorSlice from "./ApiError";

const rootReducer = combineReducers({
  user: userSlice,
  error: errorSlice,
});

export const store = configureStore({
  reducer: rootReducer,
});
