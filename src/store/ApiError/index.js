import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  error: "",
};

const errorSlice = createSlice({
  name: "error",
  initialState: initialState,
  reducers: {
    setError(state, action) {
      state.error = action.payload;
    },
    setUnauthorized() {
      localStorage.removeItem('token')
      window.location.href = '/'
      location.reload()
    },
  },
});

export const { setError, setUnauthorized } = errorSlice.actions;

export default errorSlice.reducer;
