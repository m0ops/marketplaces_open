import axios from "axios";
import { setUnauthorized } from "../store/ApiError";
import { store } from "../store";

export function createClient(config) {
  return axios.create({
    baseURL: "http://62.109.30.85:8000/",
    // baseURL: "/api/v1",
    responseType: "json",
    ...config,
  });
}

export const ApiClient = createClient();

ApiClient.interceptors.request.use(
  (config) => {
    const accessToken = localStorage.getItem("token");
    if (accessToken) {
      config.headers.Authorization = `Token ${accessToken}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

ApiClient.interceptors.response.use(
  (res) => res,
  (e) => {
    console.log(e);
    if (e.response?.status === 401) {
      store.dispatch(setUnauthorized());
    }

    return Promise.reject(e);
  }
);
