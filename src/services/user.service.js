import { userSetAccountData, userSetAuth, userSetNAR } from "../store/User";
import { ApiClient } from "./base.service";

export const userService = {
  login: (data) => {
    return ApiClient.post("/auth/otp/login/", data);
  },
  checkLoginCode: (data) => {
    return ApiClient.post("/auth/otp/check-login-code/", data);
  },
  register: (data) => {
    return ApiClient.post("/auth/users/", data);
  },
  getUserData: () => {
    return ApiClient.get("/auth/users/me/");
  },
  editPersonalData: (data) => {
    return ApiClient.patch("/auth/users/me/", data);
  },
  generateVerifyPhone: (data) => {
    return ApiClient.post("/auth/otp/check-phone/", data);
  },
  validateVerifyPhone: (data) => {
    return ApiClient.post("/auth/otp/check-phone-code/", data);
  },
  logout: () => {
    return ApiClient.post("/auth/token/logout/");
  },
};
