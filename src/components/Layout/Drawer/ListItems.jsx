import React from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { makeStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import DashboardIcon from "@material-ui/icons/Dashboard";

import Icon from "@material-ui/core/Icon";
import SummaryIcon from "../../../static/assets/icons/Drawer/categories.svg";
import icon_categories from "../../../static/assets/icons/Drawer/categories.svg";
import icon_analitics from "../../../static/assets/icons/Drawer/note.svg";
import icon_bag from "../../../static/assets/icons/Drawer/note.svg";
import icon_note from "../../../static/assets/icons/Drawer/note.svg";
import icon_chart from "../../../static/assets/icons/Drawer/note.svg";
import icon_chat from "../../../static/assets/icons/Drawer/note.svg";
import icon_settings from "../../../static/assets/icons/Drawer/note.svg";
import { useHistory } from "react-router-dom";
import { useEffect } from "react";
// import { items } from "./items";

const useStyles = makeStyles((theme) => ({
  root: {
    overflowX: "hidden",
  },
  listItem: {
    color: "#fff",
    marginLeft: 20,
    borderRadius: 16,
    height: 55,
    // width: `calc(100% - 20px)`,
  },
  listItemSelected: {
    color: theme.palette.primary.main,
    backgroundColor: "#fff !important",
  },
}));

export const ListItems = ({ handleLoginModal, handleRegisterModal }) => {
  const [selectedIndex, setSelectedIndex] = React.useState("settings");
  const classes = useStyles();
  const history = useHistory();

  const handleListItemClick = (id) => {
    setSelectedIndex(id);
  };

  useEffect(() => {
    if (selectedIndex !== "login" && selectedIndex !== "register") {
      history.push(selectedIndex);
    }
  }, [selectedIndex]);

  const onClickLoginButton = (id) => {
    handleListItemClick(id);
    handleLoginModal(true);
  };

  const onClickRegisterButton = (id) => {
    handleListItemClick(id);
    handleRegisterModal(true);
  };

  const items = [
    {
      icon: (
        <Icon>
          <img src={icon_analitics} />
        </Icon>
      ),
      name: "Сводка",
      onClick: handleListItemClick,
      id: "summary",
    },
    {
      icon: (
        <Icon>
          <img src={icon_analitics} />
        </Icon>
      ),
      name: "Аналитика",
      onClick: handleListItemClick,
      id: "analitics",
    },
    {
      icon: (
        <Icon>
          <img src={icon_categories} />
        </Icon>
      ),
      name: "Отчетность",
      onClick: handleListItemClick,
      id: "reporting",
    },
    {
      icon: (
        <Icon>
          <img src={icon_categories} />
        </Icon>
      ),
      name: "Мои товары",
      onClick: handleListItemClick,
      id: "my-goods",
    },
    {
      icon: (
        <Icon>
          <img src={icon_categories} />
        </Icon>
      ),
      name: "Склады",
      onClick: handleListItemClick,
      id: "storages",
    },
    {
      icon: (
        <Icon>
          <img src={icon_categories} />
        </Icon>
      ),
      name: "Обработка заказов",
      onClick: handleListItemClick,
      id: "order-processing",
    },
    {
      icon: (
        <Icon>
          <img src={icon_categories} />
        </Icon>
      ),
      name: "Чат с продавцом",
      onClick: handleListItemClick,
      id: "chat",
    },
    {
      icon: (
        <Icon>
          <img src={icon_categories} />
        </Icon>
      ),
      name: "Настройки",
      onClick: handleListItemClick,
      id: "settings",
    },
    {
      icon: (
        <Icon>
          <img src={icon_categories} />
        </Icon>
      ),
      name: "Вход",
      onClick: onClickLoginButton,
      id: "login",
    },
    {
      icon: (
        <Icon>
          <img src={icon_categories} />
        </Icon>
      ),
      name: "Регистрация",
      onClick: onClickRegisterButton,
      id: "register",
    },
  ];

  return (
    <List component="nav" className={classes.root}>
      {items.map((item) => (
        <ListItem
          key={`nav-link-${item.id}`}
          button
          classes={{
            root: classes.listItem,
            selected: classes.listItemSelected,
          }}
          onClick={() => item.onClick(item.id)}
          selected={selectedIndex === item.id}
        >
          <ListItemIcon>{item.icon}</ListItemIcon>
          <ListItemText primary={item.name} />
        </ListItem>
      ))}
    </List>
  );
};

ListItems.propTypes = {
  handleLoginModal: PropTypes.func,
  handleRegisterModal: PropTypes.func,
};
