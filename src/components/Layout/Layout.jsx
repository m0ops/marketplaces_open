import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import { MyDrawer as Drawer } from "./Drawer/Drawer";
import Container from "@material-ui/core/Container";

import { CustomDialog } from "../Auth/Dialog/Dialog";
import { Register } from "../Auth/Register/Register";
import { Login } from "../Auth/Login/Login";
import { Routes } from "../Content/Routes";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  content: {
    flexGrow: 1,
    height: "100vh",
    background: "white",
    overflow: "auto",
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
    paddingLeft: 50,
    paddingRight: 50,
  },
}));

export const Layout = () => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const [openLogin, setOpenLogin] = React.useState(false);
  const [openRegister, setOpenRegister] = React.useState(false);

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Drawer
        open={open}
        setOpen={setOpen}
        setOpenLogin={setOpenLogin}
        setOpenRegister={setOpenRegister}
      />

      <main className={classes.content}>
        <Container maxWidth={false} className={classes.container}>
          <Routes />
        </Container>
      </main>

      <CustomDialog open={openLogin} setOpen={setOpenLogin}>
        <Login setOpenLogin={setOpenLogin} setOpenRegister={setOpenRegister} />
      </CustomDialog>
      <CustomDialog open={openRegister} setOpen={setOpenRegister}>
        <Register
          setOpenLogin={setOpenLogin}
          setOpenRegister={setOpenRegister}
        />
      </CustomDialog>
    </div>
  );
};
