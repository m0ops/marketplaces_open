import React from "react";

import "./button.scss";

const Button = ({ className, icon, text, func, accent }) => (
  <div
    className={`btn ${accent ? "btn_accent" : ""} ${
      className ? className : ""
    }`}
    onClick={func}
  >
    {icon ? <img className="btn__icon" src={icon} /> : <></>}
    <p className="btn__text">{text}</p>
  </div>
);

export default Button;
