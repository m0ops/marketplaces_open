import React, { useEffect, useState } from "react";
import TextField from "@material-ui/core/TextField";

import "./input.scss";
const Input = ({ value, setValue, className, id, ...props }) => {
  
  const { label, helperText, isValidated, placeholder, variant, type } = props;

  return (
    <>
      {isValidated ? (
        <TextField
          id={id}
          label={label}
          onChange={(e) => setValue(e.target.value)}
          className={className}
          // InputLabelProps={{ shrink: true }}
          placeholder={placeholder}
          type={type}
          variant={variant}
        />
      ) : (
        <TextField
          error
          id={id}
          label={label}
          defaultValue={value}
          onChange={(e) => setValue(e.target.value)}
          className={className}
          placeholder={placeholder}
          InputLabelProps={{ shrink: true }}
          helperText={helperText}
          type={type}
          variant={variant}
        />
      )}
    </>
  );
};

export default Input;
