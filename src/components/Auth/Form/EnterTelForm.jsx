import React from "react";
import Box from "@material-ui/core/Box";
import { SubmitButton, TextInput, Label, EmptyTextError } from "./style";
import { useInput, isInvalid } from "./validForm";
import { userService } from "../../../services/user.service";

export const EnterTelForm = ({ classes, setCodeForm, setPk, inputTel }) => {
  const [codeRequested, setCodeRequested] = React.useState(false);

  const clickCodeRequest = async (flag) => {
    if (codeRequested) {
      setCodeForm(flag);
      return;
    }
    setCodeRequested(true);
    try {
      const res = await userService.login({ phone_number: inputTel.value });
      setPk(res.data.pk);
      setCodeForm(flag);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <Box className={classes.form}>
      <Box className={classes.inputBox}>
        <Label component="div" style={{ marginBottom: "8px" }}>
          Введите номер телефона
          {inputTel.getErrorEmpty()}
        </Label>
        <TextInput
          value={inputTel.value}
          onBlur={(e) => {
            inputTel.onBlur(e);
          }}
          onChange={(e) => {
            inputTel.onChange(e);
          }}
          error={inputTel.isEmpty && inputTel.isAfterClick}
        />
      </Box>
      <SubmitButton
        disableRipple
        variant="outlined"
        onClick={() => {
          if (!isInvalid({ inputTel })) clickCodeRequest(true);
        }}
      >
        Получить код
      </SubmitButton>
    </Box>
  );
};
