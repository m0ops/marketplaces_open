import React from "react";
import Box from "@material-ui/core/Box";
import { SubmitButton, TextInput, Label, LinkLabel } from "./style";
import { useInput, isInvalid } from "./validForm";
import { userService } from "../../../services/user.service";
import { useEffect } from "react";

export const SignUpForm = ({ classes, fetchData }) => {
  const [confirm, setConfirm] = React.useState(false);
  const [serverConfirm, setServerConfirm] = React.useState(false);
  const inputName = useInput("", { isEmpty: true });
  const inputSurname = useInput("", { isEmpty: true });
  const inputEmail = useInput("", { isEmpty: true, isInValidEmail: true });
  const inputTel = useInput("", { isEmpty: true });
  const confirmTel = useInput("", { isEmpty: true });
  const [pk, setPk] = React.useState(13);

  const onClickFetch = () => {
    if (
      isInvalid({
        inputName,
        inputSurname,
        inputEmail,
        inputTel,
        // confirmTel,
      })
    ) {
      console.log(123)
      return;
    }

    fetchData({
      first_name: inputName.value,
      last_name: inputSurname.value,
      email: inputEmail.value,
      phone_number: inputTel.value,
    });
    // console.log(isAnyError);
  };

  const generateValidateForm = async () => {
    if (
      isInvalid({
        inputTel,
      })
    ) {
      return;
    }
    try {
      const res = await userService.generateVerifyPhone({
        phone_number: inputTel.value,
      });
      setConfirm(!confirm);
      setPk(res.data.pk);
    } catch (err) {
      console.log(err);
    }
  };

  const checkTelCode = async () => {
    try {
      const res = await userService.validateVerifyPhone({
        pk: pk,
        otp: confirmTel.value,
      });
      setServerConfirm(true);
      setConfirm(!confirm);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    const timeOutId = setTimeout(() => {
      if (confirmTel.value) {
        checkTelCode();
      }
    }, 1000);
    return () => clearTimeout(timeOutId);
  }, [confirmTel.value]);

  return (
    <Box className={classes.form}>
      <Box className={classes.inputBox}>
        <Label component="div" style={{ marginBottom: "8px" }}>
          Имя
          {inputName.getErrorEmpty()}
        </Label>
        <TextInput
          onBlur={(e) => {
            inputName.onBlur(e);
          }}
          onChange={(e) => {
            inputName.onChange(e);
          }}
          error={inputName.isEmpty && inputName.isAfterClick}
        />
      </Box>
      <Box className={classes.inputBox}>
        <Label component="div" style={{ marginBottom: "8px" }}>
          Фамилия
          {inputSurname.getErrorEmpty()}
        </Label>
        <TextInput
          onBlur={(e) => {
            inputSurname.onBlur(e);
          }}
          onChange={(e) => {
            inputSurname.onChange(e);
          }}
          error={inputSurname.isEmpty && inputSurname.isAfterClick}
        />
      </Box>
      <Box className={classes.inputBox}>
        <Label component="div" style={{ marginBottom: "8px" }}>
          E-mail
          {inputEmail.getErrorEmpty() || inputEmail.getErrorInvalidEmail()}
        </Label>
        <TextInput
          onBlur={(e) => {
            inputEmail.onBlur(e);
          }}
          onChange={(e) => {
            inputEmail.onChange(e);
          }}
          error={
            (inputEmail.isEmpty && inputEmail.isAfterClick) ||
            (inputEmail.isInValidEmail && inputEmail.isAfterClick)
          }
        />
      </Box>
      <Box className={classes.inputBox} style={{ marginBottom: "30px" }}>
        <Label component="div" style={{ marginBottom: "8px" }}>
          Номер телефона
          {inputTel.getErrorEmpty()}
        </Label>
        <TextInput
          style={{ marginBottom: "10px" }}
          onBlur={(e) => {
            inputTel.onBlur(e);
          }}
          onChange={(e) => {
            inputTel.onChange(e);
          }}
          error={inputTel.isEmpty && inputTel.isAfterClick}
        />
        {!serverConfirm ? (
          <></>
        ) : (
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <LinkLabel component="div" href="#" onClick={generateValidateForm}>
              Подтвердить номер
            </LinkLabel>
            {confirmTel.getErrorEmpty()}
          </div>
        )}

        {confirm ? (
          <TextInput
            value={confirmTel.value}
            style={{ marginTop: "10px" }}
            onBlur={(e) => {
              confirmTel.onBlur(e);
            }}
            onChange={(e) => {
              confirmTel.onChange(e);
            }}
            error={confirmTel.isEmpty && confirmTel.isAfterClick}
          />
        ) : null}
      </Box>
      {!serverConfirm ? (
        <SubmitButton disableRipple variant="outlined" onClick={onClickFetch}>
          Зарегистрироваться
        </SubmitButton>
      ) : (
        <SubmitButton disableRipple variant="outlined" disabled>
          Зарегистрироваться
        </SubmitButton>
      )}
    </Box>
  );
};
