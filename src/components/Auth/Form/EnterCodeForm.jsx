import React, { useState } from "react";
import Box from "@material-ui/core/Box";
import { SubmitButton, TextInput, Label } from "../Login/style";
import { userService } from "../../../services/user.service";
import { useDispatch } from "react-redux";
import { userActions } from "../../../store/User";

export const EnterCodeForm = ({ classes, pk, setOpenLogin }) => {
  const [inputCode, setInputCode] = useState({
    0: "",
    1: "",
    2: "",
    3: "",
  });
  const dispatch = useDispatch();
  const handleOnChange = (val, ind) => {
    setInputCode((inputCode) => {
      let obj = { ...inputCode };
      obj[ind] = val;
      return obj;
    });
    const inputCode = document.querySelectorAll(".MuiInputBase-input");
    for (let i = 1; i < inputCode.length; i++) {
      const input = inputCode[i - 1];
      const nextInput = inputCode[i];
      if (input.value && !nextInput.value) {
        nextInput.focus();
        input.blur();
        break;
      }
    }
  };

  const setUserData = async () => {
    try {
      const res = await userService.getUserData();
      const { data } = res;
      dispatch(userActions.setUserData(data));
    } catch (err) {
      console.log(err);
    }
  };

  const clickCodeFetch = async () => {
    try {
      const res = await userService.checkLoginCode({
        pk: pk,
        otp: Object.keys(inputCode)
          .map((i) => inputCode[i])
          .join(""),
      });
      const { data } = res;
      dispatch(userActions.setLastLogin(data.last_login));
      localStorage.setItem("token", data.token);
      // dispatch(userActions.setToken(data.token));
      setUserData();
      setOpenLogin(false);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <Box>
      <Label component="p" style={{ marginTop: "10px" }}>
        На введенный номер телефона придет код в SMS
      </Label>
      <Box className={classes.codeForm}>
        <Label component="p">Введите код</Label>
        <Box className={classes.codeInputBox}>
          <TextInput
            inputProps={{ maxLength: 1, autoFocus: true }}
            style={{ width: "52px", marginRight: "8px" }}
            onChange={(e) => handleOnChange(e.target.value, 0)}
          />
          <TextInput
            inputProps={{ maxLength: 1 }}
            style={{ width: "52px", marginRight: "8px" }}
            onChange={(e) => handleOnChange(e.target.value, 1)}
          />
          <TextInput
            inputProps={{ maxLength: 1 }}
            style={{ width: "52px", marginRight: "8px" }}
            onChange={(e) => handleOnChange(e.target.value, 2)}
          />
          <TextInput
            inputProps={{ maxLength: 1 }}
            style={{ width: "52px" }}
            onChange={(e) => handleOnChange(e.target.value, 3)}
          />
        </Box>
        <SubmitButton disableRipple variant="outlined" onClick={clickCodeFetch}>
          Проверить код
        </SubmitButton>
      </Box>
    </Box>
  );
};
