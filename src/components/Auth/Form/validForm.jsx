import React from "react";
import { EmptyTextError } from "./style";

const guid = () => {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }

  return (
    s4() +
    s4() +
    "-" +
    s4() +
    "-" +
    s4() +
    "-" +
    s4() +
    "-" +
    s4() +
    s4() +
    s4()
  );
};

export const useValidation = (value, validations) => {
  const [isEmpty, setEmptyError] = React.useState(true);
  const [isInValidEmail, setInvalidEmailError] = React.useState(true);

  React.useEffect(() => {
    for (const validation in validations) {
      switch (validation) {
        case "isEmpty":
          value ? setEmptyError(false) : setEmptyError(true);
          break;

        case "isInValidEmail":
          const re =
            /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          re.test(value)
            ? setInvalidEmailError(false)
            : setInvalidEmailError(true);
          break;
      }
    }
  }, [value]);

  return {
    isEmpty,
    isInValidEmail,
  };
};

export const useInput = (initialValue, validations) => {
  const [value, setValue] = React.useState(initialValue);
  const [isBlur, setBlur] = React.useState(false);
  const [isAfterClick, setAfterClick] = React.useState(false);
  const valid = useValidation(value, validations);

  const onChange = (e) => {
    setValue(e.target.value);
  };

  const onBlur = (e) => {
    setBlur(true);
  };

  const onAfterClick = (status) => {
    setAfterClick(status);
  };

  const getErrorEmpty = () => {
    return valid.isEmpty && isAfterClick ? (
      <EmptyTextError>Заполните поле</EmptyTextError>
    ) : null;
  };

  const getErrorInvalidEmail = () => {
    return valid.isInValidEmail && isAfterClick ? (
      <EmptyTextError>E-mail не верный</EmptyTextError>
    ) : null;
  };

  return {
    value,
    onChange,
    onBlur,
    onAfterClick,
    getErrorEmpty,
    getErrorInvalidEmail,
    isBlur,
    isAfterClick,
    ...valid,
  };
};

export const isInvalid = (inputs) => {
  let isAnyError = false;
  for (const input in inputs) {
    inputs[input].onAfterClick(true);
    isAnyError = isAnyError || inputs[input].isEmpty;
    if (input === "inputEmail")
      isAnyError = isAnyError || inputs[input].isInValidEmail;
  }

  // isAnyError ? alert('ошибка') : alert('хорошо');
  return isAnyError;
};
