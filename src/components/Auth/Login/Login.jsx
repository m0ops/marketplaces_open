import React from "react";
import Box from "@material-ui/core/Box";
import {
  useStyles,
  Label,
  TitleBox,
  LinkLabel,
  BackIcon,
  CloseIcon,
} from "./style";
import { Typography } from "@material-ui/core";
import { EnterCodeForm } from "../Form/EnterCodeForm";
import { EnterTelForm } from "../Form/EnterTelForm";
import { useInput } from "../Form/validForm";

const LogoBox = (props) => {
  const { classes, backIcon, setCodeForm, setOpen, ...other } = props;
  return (
    <Box className={classes.logoBox}>
      {backIcon ? (
        <BackIcon
          onClick={() => {
            setCodeForm(false);
          }}
        />
      ) : (
        <></>
      )}
      <CloseIcon
        onClick={() => {
          setOpen(false);
        }}
      />
      {/* <Box component="p" className={classes.icon}>
        Лого
      </Box> */}
    </Box>
  );
};

export const Login = (props) => {
  const classes = useStyles();
  const [codeForm, setCodeForm] = React.useState(false);
  const [backIcon, setBackIcon] = React.useState(false);
  const [time, setTime] = React.useState(59);

  const [pk, setPk] = React.useState(15);
  const inputTel = useInput("", { isEmpty: true });
  React.useEffect(() => {
    if (codeForm) setBackIcon(true);
    else setBackIcon(false);
  }, [codeForm]);

  return (
    <div>
      <LogoBox
        classes={classes}
        backIcon={backIcon}
        setCodeForm={setCodeForm}
        setOpen={props.setOpenLogin}
      />
      <Box className={classes.loginBox}>
        <TitleBox>Вход</TitleBox>
        {!codeForm ? (
          <EnterTelForm
            classes={classes}
            setPk={setPk}
            setCodeForm={setCodeForm}
            inputTel={inputTel}
          />
        ) : (
          <EnterCodeForm
            classes={classes}
            setCodeForm={setCodeForm}
            pk={pk}
            setOpenLogin={props.setOpenLogin}
            setOpenRegister={props.setOpenRegister}
          />
        )}
        <Label
          component="p"
          style={{ color: "#787878", fontSize: "12px", marginBottom: "30px" }}
        >
          Повторить отправку кода можно через {time} с
        </Label>
        <Typography
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Label component="span" style={{ marginRight: "10px" }}>
            Еще нет аккаунта?
          </Label>
          <LinkLabel
            component="span"
            onClick={() => {
              props.setOpenLogin(false);
              props.setOpenRegister(true);
            }}
          >
            Зарегистрироваться
          </LinkLabel>
        </Typography>
      </Box>
    </div>
  );
};
