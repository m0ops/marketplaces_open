import React from "react";
import Dialog from "@material-ui/core/Dialog";
import { Login } from "../Login/Login";
import { Register } from "../Register/Register";
import { makeStyles, withStyles } from "@material-ui/styles";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import { useStyles, BackIcon } from "./style";
import { Box, IconButton, Typography } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";

export const CustomDialog = (props) => {
  const classes = useStyles();
  const { open, setOpen } = props;

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
        classes={{
          root: classes.root,
          paper: classes.paper,
          container: classes.container,
        }}
      >
        {props.children}
      </Dialog>
    </div>
  );
};
