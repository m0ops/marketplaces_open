import { makeStyles, withStyles } from "@material-ui/styles";


export const useStyles = makeStyles((theme) => ({
    root: {
      position: 'relative',
      minHeight: '100vh',
      backgroundColor: '#fff',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      boxSizing: 'border-box',
      fontFamily: 'Inter, sans-serif',
      padding: '10px 0',
    },
    paper: {
      position: 'relative',
      width: '541px',
      minHeight: '585px',
      borderRadius: '29px',
      overflow: 'hidden',
    },
    container: {
      height: '1000px',
    }
  }));

