import React from "react";
import Box from "@material-ui/core/Box";
import { useStyles, Label, TitleBox, LinkLabel, CloseIcon } from "./style";
import { Typography } from "@material-ui/core";
import { SignUpForm } from "../Form/SignUpForm";
import { userService } from "../../../services/user.service";

const handleCloseIconClick = (setOpenRegister) => {
  setOpenRegister(false);
};

const LogoBox = ({ classes, setOpenRegister, ...props }) => (
  <Box className={classes.logoBox}>
    {/* <Box component="p" className={classes.icon}>
      Лого
    </Box> */}
    <CloseIcon
      onClick={() => {
        handleCloseIconClick(setOpenRegister);
      }}
    />
  </Box>
);

export const Register = (props) => {
  const classes = useStyles();

  const fetchData = async (data) => {
    try {
      console.log(data);
      const res = await userService.register({ ...data });

      props.setOpenLogin(true);
      props.setOpenRegister(false);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div>
      <LogoBox classes={classes} setOpenRegister={props.setOpenRegister} />
      <Box className={classes.loginBox}>
        <TitleBox>Регистрация</TitleBox>
        <SignUpForm classes={classes} fetchData={fetchData} />
        <Typography>
          <Label component="span" style={{ marginRight: "10px" }}>
            Уже есть аккаунт?
          </Label>
          <LinkLabel
            component="span"
            onClick={() => {
              props.setOpenLogin(true);
              props.setOpenRegister(false);
            }}
          >
            Войти
          </LinkLabel>
        </Typography>
      </Box>
    </div>
  );
};
