import { Button, InputBase, Link, Typography, Box } from "@material-ui/core";
import { makeStyles, withStyles } from "@material-ui/styles";
import closeIconPng from '../../../static/assets/img/close-icon.png'


export const useStyles = makeStyles((theme) => ({
    root: {
      position: 'relative',
      minHeight: '100vh',
      backgroundColor: '#fff',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      boxSizing: 'border-box',
      fontFamily: 'Inter, sans-serif',
      paddingTop: '20px',
    },
    container: {
      position: 'relative',
      border: '1px solid rgba(0, 0, 0, 0.123)',
      width: '541px',
      height: '100%',
      boxShadow: '0px 47px 179px -33px rgba(77, 18, 0, 0.13)',
      borderRadius: '29px',
    },
    logoBox: {
      position: 'relative',
      width: '100%',
      height: '205px',
      background: 'linear-gradient(162.42deg, #F98A67 0.8%, #FFB29A 98.37%)',
      borderRadius: '16px 16px 0 0',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
    icon: {
      width: '118px',
      height: '118px',
      background: '#FFE8E0',
      borderRadius: '50%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
    loginBox: {
        position: 'relative',
        width: '100%',
        padding: '40px 70px',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    form : {
        width: '100%',
        marginTop: '30px',
    },
    text12: {
        fontWeight: 'normal',
        fontSize: '12px',
        lineHeight: '15px',
        color: '#787878',
    },
    inputBox: {
        width: '100%',
        marginBottom: '15px',
    },
  }));

  export const Label = withStyles({
    root: {
        fontWeight: 'normal',
        fontSize: '16px',
        lineHeight: '19px',
        color: '#2F2F2F',
    },
  })(Typography);

  export const LinkLabel = withStyles({
    root: {
        fontWeight: '600',
        fontSize: '16px',
        lineHeight: '19px',
        color: '#F98A67',
        transition: '300ms',
        textDecoration: 'none',
        cursor: 'pointer',
        '&:hover': {
            color: '#FF9F81',
            textDecoration: 'underline',
        },
        '&:active': {
            color: '#F4805B',
            textDecoration: 'underline',
        },
    },
  })(Link);

  export const TitleBox = withStyles({
    root: {
        fontWeight: '600',
        fontSize: '30px',
        lineHeight: '36px',
        color: '#2F2F2F',
    },
  })(Typography);

  export const CloseIcon = withStyles({
    root: {
        position: 'absolute',
        top: '30px',
        right: '30px',
        width: '16px',
        height: '16px',
        backgroundImage: `url(${closeIconPng})`,
        cursor: 'pointer',
        transition: '300ms',
        '&:hover': {
            opacity: '.55',
        },
        '&:active': {
            opacity: '.95',
        },
    },
  })(Box);

  export const SubmitButton = withStyles({
      root: {
        width: '100%',
        height: '55px',
        padding: '18px 37px',
        background: '#F98A67',
        border: 'none',
        borderRadius: '16px',
        fontWeight: 'normal',
        fontSize: '16px',
        lineHeight: '19px',
        textAlign: 'center',
        color: '#FFFFFF',
        marginBottom: '10px',
        textTransform: 'none',
        '&:hover': {
            backgroundColor: '#FF9F81',
        },
        '&:active': {
            backgroundColor: '#F4805B',
        },
      }
  })(Button);

  export const TextInput = withStyles({
    root: {
        width: '100%',
        height: '58px',
        background: '#FFF7F4',
        border: '1px solid #FFE8E0',
        borderRadius: '16px',
        padding: '0 10px',
        fontSize: '26px', 
        '& input': {
          textAlign: "center",
        },
        '&:hover': {
            border: '1px solid #FFCBBA',
        },
        '&.Mui-focused': {
            border: '1px solid #F98A67',
        },
    }
  })(InputBase);
