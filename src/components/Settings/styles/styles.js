import { InputBase } from "@material-ui/core";
import { makeStyles, withStyles } from "@material-ui/styles";

export const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  button: {
    padding: "18px 50px",
    width: 309,
    minWidth: 262,
    whiteSpace: "nowrap",
    borderColor: theme.palette.primary.main,
    borderRadius: "16px",
    height: 62,
    "& > span": {
      textTransform: "initial",
      color: theme.palette.primary.main,
    },
  },
  buttonWrapper: {
    display: "flex",
    justifyContent: "flex-end",
  },
  blockTitle: {
    marginBottom: 20,
    marginTop: 20,
  },
  personalBlockWrapper: {
    minWidth: 690,
    flexDirection: "column",
  },
  accountBlockWrapper: {
    minWidth: 390,
    flexDirection: "column",
    paddingLeft: 10,
  },
  blockWrapper: {
    flexDirection: "column",
  },
  personalBlock: {
    display: "flex",
    justifyContent: "space-between",
    // width: "60%",
    minWidth: "700px",
    maxWidth: "991px",
    height: 364,
    background: "#FFF7F4",
    borderRadius: "16px",
    padding: 20,
  },
  topBlock: {
    display: "flex",
    justifyContent: "space-between",
    width: "100%",
  },
}));
export const TextInput = withStyles({
  root: {
    width: "100%",
    height: "58px",
    background: "#FFF7F4",
    border: "1px solid #FFE8E0",
    borderRadius: "16px",

    padding: "0 10px",
    fontSize: "26px",
    textAlign: "center",
    "& input": {
      textAlign: "center",
    },
    "&:hover": {
      border: "1px solid #FFCBBA",
    },
    "&.Mui-focused": {
      border: "1px solid #F98A67",
    },
    "&.Mui-error": {
      backgroundColor: "#FFDBDB",
      borderColor: "#FF2F2F",
    },
  },
})(InputBase);
