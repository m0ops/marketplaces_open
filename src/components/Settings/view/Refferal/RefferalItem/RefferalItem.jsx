import React from "react";
import Box from "@material-ui/core/Box";
import { makeStyles, withStyles } from "@material-ui/styles";
import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    marginTop: 10,
    height: 200,
    background: "linear-gradient(162.42deg, #FFDACE 0.8%, #FFB29A 98.37%)",
    borderRadius: 16,
    padding: 20,
  },
}));

export const RefferalItem = ({ title, text, num, type }) => {
  const classes = useStyles();

  return (
    <Box component="div" className={classes.root}>
      <Typography variant="h6" component="h5">
        {title}
      </Typography>
      <Typography variant="subtitle2" component="h6">
        {text}
      </Typography>
      <Typography variant="h6" component="h6">
        {num}
      </Typography>
    </Box>
  );
};
