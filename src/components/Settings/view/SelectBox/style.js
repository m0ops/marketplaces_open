import { Box, Button, Container, InputBase, Link, Typography } from "@material-ui/core";
import { makeStyles, withStyles } from "@material-ui/styles";
import arrowIcon from '../../../../static/assets/img/arrow.png';

export const useStyles = makeStyles((theme) => ({
    select: {
      display: 'flex',
      justifyContent: 'left',
      alignItems: 'center',
      width: '100%',
      height: '58px',
      background: '#FFF7F4',
      border: '1px solid #FFE8E0',
      borderRadius: '16px',
      padding: '19px 18px',
      fontSize: '16px', 
      cursor: 'pointer',
      '&:hover': {
        border: '1px solid #FFCBBA',
      },
      '&:active': {
          border: '1px solid #F98A67',
      },
      '&::before': {
        position: 'relative',
        display: 'inline-block',
        boxSizing: 'border-box',
        width: '24px',
        height: '24px',
        content: '""',
        left: '95%',
        backgroundImage: `url(${arrowIcon})`,
        backgroundSize: '8px 14px',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        cursor: 'pointer',
      },
    },
    rotate: {
      '&::before': {
        transform: 'rotate(90deg)',
      },
    },
    selectMenu: {
      position: 'absolute',
      width: '300px',
      opacity: '0',
      background: '#FFF7F4',
      border: '1px solid #FFCBBA',
      borderRadius: '0 0 16px 16px',
      padding: '10px',
      display: 'none',
      zIndex: '100',
      cursor: 'pointer',
    },
    selectItem: {
      listStyle: 'none',
      padding: '19px 18px',
      cursor: 'pointer',
      borderBottom: '1px solid #FFCBBA',
      '&:hover': {
        background: '#fff5ee',
      },
      '&:last-child': {
        borderBottom: 'none',
        borderRadius: '0 0 16px 16px',
      },
    },
  }));
