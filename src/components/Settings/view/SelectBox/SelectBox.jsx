import React from "react";
import clsx from "clsx";
import { useStyles } from "./style.js";

const handleSelectClick = (isClick) => { 
    const selectButton = document.querySelector('#setting-selectButton');
    const selectMenu = document.querySelector('#setting-selectMenu');
    if (isClick) {
        selectMenu.style.opacity = '1';
        selectMenu.style.display = 'block';
        selectButton.style.borderRadius = '16px 16px 0px 0px';
    } else {
        selectMenu.style.opacity = '0';
        selectMenu.style.display = '';
        selectButton.style.borderRadius = '16px';
    }
    
}

const handleSelectItemClick = (event, setName) => { 
    //const selectItem = document.querySelector('.makeStyles-selectMenu-7');
    const selectButton = document.querySelector('#setting-selectButton');
    const selectMenu = document.querySelector('#setting-selectMenu');
    selectMenu.style.opacity = '0';
    selectMenu.style.display = '';
    selectButton.style.borderRadius = '16px';
    setName(event.target.textContent);
}


export const SelectBox = ({handleChange, marketPlace, ...props }) => {
    const classes = useStyles();
    const markets = ['OZON', 'WildBerrys', 'Amazon']
    const [isClick, setClick] = React.useState(false);
    const [nameMarket, setName] = React.useState('Выберите маркетплейс');

    React.useEffect(() => {
        handleSelectClick(isClick)
    }, [isClick])

    return (
        <div style={{position: 'relative', width: '300px'}}>
            <button 
            id="setting-selectButton"
            className={ clsx(classes.select, isClick && classes.rotate) } 
            onClick={() => { setClick(!isClick) }}>
                {nameMarket}
            </button>
            <ul className={classes.selectMenu} id="setting-selectMenu">
                {markets.map((market, i) => (
                    <li 
                    key={i} 
                    className={classes.selectItem} 
                    onClick={(event) => { 
                        handleSelectItemClick(event, setName), 
                        setClick(false) 
                    }}
                    >
                    {market}
                    </li> 
                ))}
            </ul>
        </div>
        
    );
}