import React from "react";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import { PersonalForm } from "./PersonalForm/PersonalForm";
import { InfoRow } from "./InfoRow/InfoRow";

export const PersonalBlock = ({ classes }) => {
  return (
    <Grid container spacing={1} className={classes.personalBlock}>
      <Grid container item xs={12} spacing={3}>
        <InfoRow classes={classes} />
      </Grid>
      <Grid container item xs={12} spacing={3}>
        <PersonalForm />
      </Grid>
    </Grid>
  );
};
