import React from "react";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Input from "../../../../PageBlock/Input/Input";
import balanceIcon from "../../../../../static/assets/icons/Settings/BalanceIcon.svg";

const useStyles = makeStyles((theme) => ({
  root: {
    background: "#FFF7F4",
    borderRadius: "16px",
    padding: 20,
    width: "48px",
    height: "48px",
  },
  button: {
    "& > span": {
      textTransform: "initial",
      color: theme.palette.primary.main,
    },
  },
  balanceCell: {
    display: "flex",
    "& > img": {
      marginRight: "10px",
    },
  },
}));
const date = "04.08.2021";
const tarifName = "Название";
const balance = 7000;

export const InfoRow = ({}) => {
  const classes = useStyles();
  return (
    <>
      {/* <Grid item xs={4}>
          <Box className={classes.paper}>
            <Button className={classes.button}>Изменить фото профиля</Button>
          </Box>
        </Grid> */}
      <Grid item xs={4}>
        <Typography
          variant="body1"
          component="p"
          style={{ whiteSpace: "nowrap" }}
        >
          Ваш тариф (до {date})
        </Typography>
        <Typography variant="h6" component="p" style={{ whiteSpace: "nowrap" }}>
          {tarifName}
        </Typography>
        <Button className={classes.button}>Продлить</Button>
      </Grid>
      <Grid item xs={4} className={classes.balanceCell}>
        <img src={balanceIcon} />
        <Box className={classes.paper}>
          <Typography
            variant="body1"
            component="p"
            style={{ whiteSpace: "nowrap" }}
          >
            Ваш баланс
          </Typography>
          <Typography
            variant="h6"
            component="p"
            style={{ whiteSpace: "nowrap" }}
          >
            {balance} р
          </Typography>
          <Button className={classes.button}>Пополнить</Button>
        </Box>
      </Grid>
    </>
  );
};
