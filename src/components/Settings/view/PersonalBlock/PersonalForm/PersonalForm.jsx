import React from "react";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Input from "../../../../PageBlock/Input/Input";
import { useState, useEffect } from "react";
import { SubmitButton, TextInput, Label, LinkLabel, useStyles } from "./style";
import { useInput, isInvalid } from "./validForm";
import { useSnackbar } from "notistack";
import { useSelector } from "react-redux";
import { userService } from "../../../../../services/user.service";

export const PersonalForm = () => {
  const classes = useStyles();
  const userData = useSelector((state) => state.user.userData);

  const inputName = useInput(userData.first_name, { isEmpty: true });
  const inputSurname = useInput(userData.last_name, { isEmpty: true });
  const inputTel = useInput(userData.phone_number, { isEmpty: true });
  const inputEmail = useInput(userData.email, {
    isEmpty: true,
    isInValidEmail: true,
  });
  const confirmTel = useInput("", { isEmpty: true });

  const [confirm, setConfirm] = useState(false);
  const [confirmMail, setConfirmMail] = useState(false);
  const [isEditName, setEditName] = useState(false);
  const [isEditLName, setEditLName] = useState(false);
  const [isEditEmail, setEditEmail] = useState(false);
  const [isEditTel, setEditTel] = useState(false);
  const [pk, setPk] = useState();

  useEffect(() => {
    inputName.setValue(userData.first_name);
    inputSurname.setValue(userData.last_name);
    inputTel.setValue(userData.phone_number);
    inputEmail.setValue(userData.email);
  }, [userData]);

  const { enqueueSnackbar } = useSnackbar();
  // Телефон  ........................................................................
  const toggleConfirmButton = () => {
    setEditTel(true);
  };

  const setTelEdit = async () => {
    try {
      const res = await userService.generateVerifyPhone({
        phone_number: inputTel.value,
      });
      setPk(res.data.pk);
      setEditTel(false);
      setConfirm(true);
    } catch (err) {
      enqueueSnackbar(`${err.response.data.detail}`, {
        variant: "error",
        autoHideDuration: 2000,
      });
      // console.log(err);
    }
  };

  const checkValidCode = async () => {
    setConfirm(false);
    try {
      const res = await userService.validateVerifyPhone({
        pk: pk,
        otp: confirmTel.value,
      });
      setConfirm(false);
      editPersonalData("Телефон", { phone_number: inputTel.value });
    } catch (err) {
      enqueueSnackbar(`${err.response.data.detail}`, {
        variant: "error",
        autoHideDuration: 2000,
      });
    }
  };
  //  .................................................................................

  // Email  ........................................................................
  const saveEmail = () => {
    setEditLName(false);
    editPersonalData("Email", {
      email: inputEmail.value,
    });
  };
  //  .................................................................................

  const editPersonalData = async (field, data) => {
    try {
      const res = await userService.editPersonalData(data);
      enqueueSnackbar(`Поле ${field} успешно изменено!`, {
        variant: "success",
        autoHideDuration: 2000,
      });
    } catch (err) {
      enqueueSnackbar("Ошибка!", {
        variant: "error",
        autoHideDuration: 2000,
      });
    }
  };

  const saveEditName = () => {
    setEditName(false);
    editPersonalData("Имя", {
      first_name: inputName.value,
    });
  };

  const saveEditLName = () => {
    setEditLName(false);
    editPersonalData("Фамилия", {
      last_name: inputSurname.value,
    });
  };

  return (
    <form className={classes.root}>
      <Box className={classes.formRow}>
        {/* Имя ..............................................................*/}
        <Box className={classes.inputBox}>
          <Label
            component="div"
            style={{ marginBottom: "8px", marginRight: "10px" }}
          >
            Имя
            {inputName.getErrorEmpty()}
          </Label>
          {isEditName ? (
            <>
              <TextInput
                value={inputName.value}
                onBlur={(e) => {
                  inputName.onBlur(e);
                }}
                onChange={(e) => {
                  inputName.onChange(e);
                }}
                error={inputName.isEmpty && inputName.isAfterClick}
              />
              <Button className={classes.button} onClick={saveEditName}>
                Сохранить
              </Button>
            </>
          ) : (
            <>
              <TextInput
                disabled
                value={inputName.value}
                onBlur={(e) => {
                  inputName.onBlur(e);
                }}
                onChange={(e) => {
                  inputName.onChange(e);
                }}
                error={inputName.isEmpty && inputName.isAfterClick}
              />
              <Button
                className={classes.button}
                onClick={() => setEditName(true)}
              >
                Изменить
              </Button>
            </>
          )}
        </Box>
        {/* Телефон ..............................................................*/}
        <Box className={classes.inputBox}>
          {isEditTel ? (
            <>
              <Label component="div" style={{ marginBottom: "8px" }}>
                Телефон
                {inputTel.getErrorEmpty()}
              </Label>
              <TextInput
                value={inputTel.value}
                onBlur={(e) => {
                  inputTel.onBlur(e);
                }}
                onChange={(e) => {
                  inputTel.onChange(e);
                }}
                error={inputTel.isEmpty && inputTel.isAfterClick}
              />
              <Button className={classes.button} onClick={setTelEdit}>
                Сохранить
              </Button>
            </>
          ) : confirm ? (
            <>
              <Label component="div" style={{ marginBottom: "8px" }}>
                Проверочный код
                {confirmTel.getErrorEmpty()}
              </Label>
              <TextInput
                value={confirmTel.value}
                onBlur={(e) => {
                  confirmTel.onBlur(e);
                }}
                onChange={(e) => {
                  confirmTel.onChange(e);
                }}
                error={confirmTel.isEmpty && confirmTel.isAfterClick}
              />
              <Button className={classes.button} onClick={checkValidCode}>
                Подтвердить
              </Button>
            </>
          ) : (
            <>
              <Label component="div" style={{ marginBottom: "8px" }}>
                Телефон
                {inputTel.getErrorEmpty()}
              </Label>
              <TextInput
                disabled
                value={inputTel.value}
                onBlur={(e) => {
                  confirmTel.onBlur(e);
                }}
                onChange={(e) => {
                  confirmTel.onChange(e);
                }}
                error={confirmTel.isEmpty && confirmTel.isAfterClick}
              />
              <Button className={classes.button} onClick={toggleConfirmButton}>
                Изменить
              </Button>
            </>
          )}
        </Box>
      </Box>
      <Box className={classes.formRow}>
        {/* Фамилия ..............................................................*/}
        <Box className={classes.inputBox}>
          <Label
            component="div"
            style={{ marginBottom: "8px", marginRight: "10px" }}
          >
            Фамилия
            {inputSurname.getErrorEmpty()}
          </Label>
          {isEditLName ? (
            <>
              <TextInput
                value={inputSurname.value}
                onBlur={(e) => {
                  inputSurname.onBlur(e);
                }}
                onChange={(e) => {
                  inputSurname.onChange(e);
                }}
                error={inputSurname.isEmpty && inputSurname.isAfterClick}
              />
              <Button className={classes.button} onClick={saveEditLName}>
                Сохранить
              </Button>
            </>
          ) : (
            <>
              <TextInput
                value={inputSurname.value}
                disabled
                onBlur={(e) => {
                  inputSurname.onBlur(e);
                }}
                onChange={(e) => {
                  inputSurname.onChange(e);
                }}
                error={inputSurname.isEmpty && inputSurname.isAfterClick}
              />
              <Button
                className={classes.button}
                onClick={() => setEditLName(true)}
              >
                Изменить
              </Button>
            </>
          )}
        </Box>
        {/* Емаил ...............................................................*/}
        <Box className={classes.inputBox}>
          <Label component="div" style={{ marginBottom: "8px" }}>
            E-mail
            {inputEmail.getErrorEmpty() || inputEmail.getErrorInvalidEmail()}
          </Label>
          {isEditEmail ? (
            <>
              <TextInput
                value={inputEmail.value}
                onBlur={(e) => {
                  inputEmail.onBlur(e);
                }}
                onChange={(e) => {
                  inputEmail.onChange(e);
                }}
                error={
                  (inputEmail.isEmpty && inputEmail.isAfterClick) ||
                  (inputEmail.isInValidEmail && inputEmail.isAfterClick)
                }
              />
              <Button className={classes.button} onClick={saveEmail}>
                Подтвердить
              </Button>
            </>
          ) : (
            <>
              <TextInput
                value={inputEmail.value}
                disabled
                onBlur={(e) => {
                  inputEmail.onBlur(e);
                }}
                onChange={(e) => {
                  inputEmail.onChange(e);
                }}
                error={
                  (inputEmail.isEmpty && inputEmail.isAfterClick) ||
                  (inputEmail.isInValidEmail && inputEmail.isAfterClick)
                }
              />
              <Button
                className={classes.button}
                onClick={() => setEditEmail(true)}
              >
                Изменить
              </Button>
            </>
          )}
        </Box>
      </Box>
    </form>
  );
};
