import {
  Box,
  Button,
  Container,
  InputBase,
  Link,
  Typography,
} from "@material-ui/core";
import { makeStyles, withStyles } from "@material-ui/styles";
// import backIconPng from "../../../static/assets/img/back-icon.png";
// import closeIconPng from "../../../static/assets/img/close-icon.png";

export const useStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: 12,
    width: "100%",
  },
  formRow: {
    width: "100%",
    display: "flex",
  },
  button: {
    "& > span": {
      textTransform: "initial",
      color: theme.palette.primary.main,
    },
  },
  formInput: {
    // maxWidth: "380px",
    width: "48%",
    minWidth: 270,
    marginRight: theme.spacing(1),
    "& > div": {
      borderRadius: 16,
    },
  },
  inputBox: {
    width: "48%",
    minWidth: 270,
    marginBottom: "8px",
    marginRight: theme.spacing(1),
    "& > div": {
      borderRadius: 16,
    },
  },
  container: {
    border: "1px solid rgba(0, 0, 0, 0.123)",
    width: "541px",
    minHeight: "585px",
    boxShadow: "0px 47px 179px -33px rgba(77, 18, 0, 0.13)",
    borderRadius: "29px",
  },
  logoBox: {
    position: "relative",
    top: "-1px",
    left: "-1px",
    width: "calc(100% + 2px)",
    height: "205px",
    background: "linear-gradient(162.42deg, #F98A67 0.8%, #FFB29A 98.37%)",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  backIcon: {
    position: "absolute",
    left: "20px",
    top: "20px",
    width: "38px",
    height: "38px",
    background: "url('images/back-icon.png')",
    cursor: "pointer",
    transition: "300ms",
  },
  icon: {
    width: "118px",
    height: "118px",
    background: "#FFE8E0",
    borderRadius: "50%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  loginBox: {
    width: "100%",
    padding: "40px 70px",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  form: {
    width: "100%",
    marginTop: "30px",
  },
  text12: {
    fontWeight: "normal",
    fontSize: "12px",
    lineHeight: "15px",
    color: "#787878",
  },

  codeForm: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    marginTop: "30px",
  },
  codeInputBox: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: "30px",
    marginTop: "8px",
  },
}));

export const Label = withStyles({
  root: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    fontWeight: "normal",
    fontSize: "16px",
    lineHeight: "19px",
    color: "#2F2F2F",
  },
})(Typography);

export const EmptyTextError = withStyles({
  root: {
    display: "inline-block",
    fontWeight: "normal",
    fontSize: "14px",
    lineHeight: "17px",
    color: "#FF2F2F",
  },
})(Typography);

export const LinkLabel = withStyles({
  root: {
    display: "inline-block",
    fontWeight: "600",
    fontSize: "16px",
    lineHeight: "19px",
    color: "#F98A67",
    transition: "300ms",
    textDecoration: "none",
    cursor: "pointer",
    "&:hover": {
      color: "#FF9F81",
      textDecoration: "underline",
    },
    "&:active": {
      color: "#F4805B",
      textDecoration: "underline",
    },
  },
})(Link);

export const TitleBox = withStyles({
  root: {
    fontWeight: "600",
    fontSize: "30px",
    lineHeight: "36px",
    color: "#2F2F2F",
  },
})(Typography);

export const SubmitButton = withStyles({
  root: {
    width: "100%",
    height: "55px",
    padding: "18px 37px",
    background: "#F98A67",
    border: "none",
    borderRadius: "16px",
    fontWeight: "normal",
    fontSize: "16px",
    lineHeight: "19px",
    textAlign: "center",
    color: "#FFFFFF",
    marginBottom: "10px",
    textTransform: "none",
    "&:hover": {
      backgroundColor: "#FF9F81",
    },
    "&:active": {
      backgroundColor: "#F4805B",
    },
  },
})(Button);

export const TextInput = withStyles({
  root: {
    width: "100%",
    height: "58px",
    background: "#FFF7F4",
    border: "1px solid #FFE8E0",
    borderRadius: "16px",
    padding: "0 10px",
    fontSize: "16px",
    textAlign: "left",
    "& input": {
      textAlign: "left",
    },
    "&:hover": {
      border: "1px solid #FFCBBA",
    },
    "&.Mui-focused": {
      border: "1px solid #F98A67",
    },
    "&.Mui-error": {
      backgroundColor: "#FFDBDB",
      borderColor: "#FF2F2F",
    },
  },
})(InputBase);
