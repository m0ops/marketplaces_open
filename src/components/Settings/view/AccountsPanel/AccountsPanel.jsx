import React from "react";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import { CustomSelect as AccountSelect } from "./Select/Select";
import { AccountList } from "./AccountList/AccountList";
import { SelectBox } from "../SelectBox/SelectBox";

export const AccountsPanel = ({}) => {
  return (
    <Box>
      <SelectBox />
      <AccountList />
    </Box>
  );
};
