import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    marginTop: 0,
    minWidth: 230,
    background: "#FFF7F4",
    borderRadius: 16,
    "& > div": {
      "& > div": {
        borderRadius: 16,
      },
      "& > fieldset": {
        borderColor: "#FFE8E0",
        borderRadius: 16,
      },
    },
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export function CustomSelect() {
  const classes = useStyles();
  const [market, setMarket] = React.useState(1);

  const handleChange = (event) => {
    setMarket(event.target.value);
  };

  return (
    <div>
      <FormControl variant="outlined" className={classes.formControl}>
        <Select
          value={market}
          onChange={handleChange}
          // label="Выберите маркет"
        >
          <MenuItem value={1}>OZON</MenuItem>
          <MenuItem value={2}>Яндекс.Маркет</MenuItem>
          <MenuItem value={3}>Wildberies</MenuItem>
        </Select>
      </FormControl>
    </div>
  );
}
