import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Grid from "@material-ui/core/Grid";
import DeleteIcon from "@material-ui/icons/Delete";
import { IconButton, ListItemSecondaryAction } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    marginTop: 0,
    height: 296,
    overflowY: "scroll",
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    margin: theme.spacing(4, 0, 2),
  },
  listItem: {
    borderBottom: "1px solid #FFE8E0",
  },
}));

const accounts = [
  "account 1",
  "account 2",
  "account 3",
  "account 4",
  "account 5",
  "account 6",
];

export const AccountList = () => {
  const classes = useStyles();
  const [dense, setDense] = React.useState(false);
  const [secondary, setSecondary] = React.useState(false);
  const handledeleteClick = (id) => {
    console.log(id);
  };
  return (
    <div className={classes.root}>
      <div className={classes.demo}>
        <List dense={dense}>
          {accounts.map((i) => (
            <ListItem key={i} className={classes.listItem}>
              <ListItemText
                primary={i}
                secondary={secondary ? "Secondary text" : null}
              />
              <ListItemSecondaryAction>
                <IconButton
                  edge="end"
                  aria-label="delete"
                  onClick={() => handledeleteClick(i)}
                >
                  <DeleteIcon />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          ))}
        </List>
      </div>
    </div>
  );
};
