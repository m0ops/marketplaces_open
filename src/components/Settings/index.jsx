import React from "react";

import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";

import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import { useStyles } from "./styles/styles";
import { PersonalBlock } from "./view/PersonalBlock/PersonalBlock";
import { AccountsPanel } from "./view/AccountsPanel/AccountsPanel";
import { RefferalItem } from "./view/Refferal/RefferalItem/RefferalItem";

import { useEffect } from "react";
import { userService } from "../../services/user.service";
import { useDispatch } from "react-redux";
import { userActions } from "../../store/User";
import { CustomTable } from "../Summary/view/custom/Table/CustomTable";
import { SelectBox } from "./view/SelectBox/SelectBox";

export const Settings = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const setUserData = async () => {
    if (localStorage.getItem("token")) {
      try {
        const res = await userService.getUserData();
        const { data } = res;
        dispatch(userActions.setUserData(data));
      } catch (err) {
        console.log(err);
      }
    }
  };

  useEffect(() => {
    setUserData();
  }, []);

  return (
    <Box component="div" className={classes.root}>
      <Box className={classes.topBlock}>
        <Typography variant="h4" component="h1">
          Личный кабинет
        </Typography>
        <Button classes={{ root: classes.button }} variant="outlined">
          Сменить пользователя
        </Button>
      </Box>

      <Grid container spacing={1}>
        <Grid xs={7} item className={classes.personalBlockWrapper}>
          <Typography
            variant="h6"
            component="h5"
            className={classes.blockTitle}
          >
            Личныe данные
          </Typography>
          <PersonalBlock classes={classes} />
        </Grid>
        <Grid xs={5} item className={classes.accountBlockWrapper}>
          <Typography
            variant="h6"
            component="h5"
            className={classes.blockTitle}
          >
            Мои аккаунты
          </Typography>
          <AccountsPanel />
        </Grid>
      </Grid>

      <Typography variant="h6" component="h5" style={{ marginTop: "50px" }}>
        Реферальная программа
      </Typography>
      <Typography
        variant="subtitle1"
        component="h6"
        style={{ marginBottom: "30px" }}
      >
        Текст о реферальной программе, объясняющий как ей пользоваться и что она
        дает
      </Typography>

      <Grid container spacing={1}>
        <Grid xs={3} item>
          <RefferalItem
            title="Прибыль"
            text="Ваша прибыль по партнерской программе"
            num={"5 000 432"}
            type=""
          />
        </Grid>
        <Grid xs={3} item>
          <RefferalItem
            title="Статистика"
            text="Количество приглашенных пользователей"
            num={"12"}
            type=""
          />
        </Grid>
        <Grid xs={3} item>
          <RefferalItem title="Выплаты" text="" num={""} type="" />
        </Grid>
      </Grid>

      <Box style={{ marginTop: "50px" }}>
        <Typography
          variant="h6"
          component="h5"
          style={{ marginBottom: "20px" }}
        >
          Ваши ссылки:{" "}
        </Typography>
        <CustomTable />
      </Box>
    </Box>
  );
};
