import { Box, Button, Container, InputBase, Link, Typography } from "@material-ui/core";
import { makeStyles, withStyles } from "@material-ui/styles";
import arrowIcon from '../../../static/assets/img/arrow.png';

export const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  fz20: {
    fontWeight: '600',
    fontSize: '20px',
    lineHeight: '24px',
    color: '#2F2F2F',
  },
  fz18: {
    fontWeight: '600',
    fontSize: '18px',
    lineHeight: '22px',
    color: '#2F2F2F',
  },
  fz16: {
    fontWeight: 'normal',
    fontSize: '16px',
    lineHeight: '19px',
    color: '#2F2F2F',
  },
  select: {
    display: 'flex',
    justifyContent: 'left',
    alignItems: 'center',
    width: '100%',
    height: '58px',
    background: '#FFF7F4',
    border: '1px solid #FFE8E0',
    borderRadius: '16px',
    padding: '19px 18px',
    fontSize: '16px', 
    cursor: 'pointer',
    '&:hover': {
      border: '1px solid #FFCBBA',
    },
    '&:active': {
        border: '1px solid #F98A67',
    },
    '&::before': {
      position: 'relative',
      display: 'inline-block',
      boxSizing: 'border-box',
      width: '24px',
      height: '24px',
      content: '""',
      left: '95%',
      backgroundImage: `url(${arrowIcon})`,
      backgroundSize: '8px 14px',
      backgroundPosition: 'center',
      backgroundRepeat: 'no-repeat',
      cursor: 'pointer',
    },
  },
  rotate: {
    '&::before': {
      transform: 'rotate(90deg)',
    },
  },
  selectMenu: {
    position: 'absolute',
    width: 'calc(100% - 60px)',
    opacity: '0',
    background: '#FFF7F4',
    border: '1px solid #FFCBBA',
    borderRadius: '0 0 16px 16px',
    padding: '10px',
    display: 'none',
    zIndex: '100',
    cursor: 'pointer',
  },
  selectItem: {
    listStyle: 'none',
    padding: '19px 18px',
    cursor: 'pointer',
    borderBottom: '1px solid #FFCBBA',
    '&:hover': {
      background: '#fff5ee',
    },
    '&:last-child': {
      borderBottom: 'none',
      borderRadius: '0 0 16px 16px',
    },
  },
  profitBox: {
    position: 'relative',
    minWidth: '246px',
    height: '56px',
    padding: '4px 32px',
    border: '1px solid #F98A67',
    boxSizing: 'border-box',
    borderRadius: '16px',
    marginRight: '20px',
    '&:last-child': {
      marginRight: '0',
    }
  },
  justContGrow: {
    display: 'flex',
  },
  btnMore: {
    border: 'none',
    background: 'transparent',
    color: '#F98A67',
    textTransform: 'none',
    fontWeight: '600',
    fontSize: '20px',
    lineHeight: '24px',
    marginTop: '20px',
  },
  btnNameThing: {
    width: '245px',
    height: '55px',
    textTransform: 'none',
    background: '#FFFFFF',
    opacity: '0.9',
    borderRadius: '16px',
    marginTop: '20px',
    justifyContent: 'flex-start',
  },
  btnEndIcon: {
    position: 'relative',
    display: 'inline-block',
    left: '75px',
    justifyContent: 'left',
    marginTop: '7px'
  },
  chartBox: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '450px',
    height: '300px',
    background: '#FFFFFF',
    border: '1px solid #FFE8E0',
    borderRadius: '16px',
    padding: '20px',
  }, 
  chartsContainer: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: '50px', 
  }, 
  imgWrapper: {
    width: '100%',
    objectPosition: 'center',
    objectSize: 'cover',
    marginLeft: '30px'
  },
  img: {
    maxWidth: '260px',
  },
  imgBox: {
    width: '353px',
    height: '300px',
    padding: '20px 15px',
    background: '#F98A67',
    borderRadius: '16px',
  }
}))
