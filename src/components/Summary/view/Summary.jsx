import React from "react";
import Box from "@material-ui/core/Box";
import { useStyles } from "./style.js";
import { SelectCheckBox } from "./custom/SelectCheckBox/SelectCheckBox.jsx";
import { Button, Icon, Typography } from "@material-ui/core";
import { LineChart } from "./custom/Chart/LineChart.jsx";
import bestSeller from "../../../static/assets/img/bestseller-thing.png"
import arrowIcon from "../../../static/assets/img/arrow.png" 
import { TabPages } from "./custom/TabPages/TabPages";

const handleClickOnBestThing = (name) => {
    alert('best thing ' + name);
}

const handleClickOnBtnInChart = (name) => {
    alert('line chart ' + name);
}

const ProfitBox = ({classes, title, value}) => (
    <Box component='div' className={classes.profitBox}>
        <Typography component='p' className={classes.fz16} style={{fontWeight: '600'}}>{title}</Typography>
        <Typography component='p' className={classes.fz16}>{value}</Typography>
    </Box>
)

const ChartBox = ({classes, title}) => (
    <Box component="div">
        <Typography 
        component='p' 
        className={classes.fz18} 
        style={{marginBottom: '10px'}}
        >
            {title}
        </Typography>
        <Box className={classes.chartBox}>
            <LineChart />
            <Button 
            disableRipple 
            variant='outlined' 
            className={classes.btnMore}
            onClick={() => { handleClickOnBtnInChart(title) }}
            >
                Подробнее
            </Button>
        </Box>                   
    </Box>
)

const BestCommodity = ({classes, commodityName}) => (
    <Box component="div">
        <Typography component='p' className={classes.fz18} style={{marginBottom: '10px'}}>
            Самый ходовой товар
        </Typography>
        <Box className={classes.imgBox}>
            <div className={classes.imgWrapper}>
                <img src={bestSeller} className={classes.img}/>
            </div>
            <Button 
            disableRipple 
            variant='outlined' 
            className={classes.btnNameThing}
            endIcon={<Icon className={classes.btnEndIcon}><img src={arrowIcon} /></Icon>}
            onClick={() => { handleClickOnBestThing(commodityName) }}
            >
                {commodityName}
            </Button>
        </Box>
    </Box>
)

const getPages = () => {
    
}

export const Summary = (props) => {
    const classes = useStyles();
    const [marketPlace, setMarket] = React.useState('Аккаунт');
    const handleChange = (value) => {
        setMarket(value);
    };

    return(
        <Box component="div" className={classes.root}>
            <Typography component='p' className={classes.fz20} style={{marginBottom: '23px'}}>
                Выберите аккаунты
            </Typography>
            <Typography component='p' className={classes.fz16} style={{marginBottom: '11px'}}>
                {marketPlace}
            </Typography>
            <Box component="div" className={classes.justContGrow}>
                <SelectCheckBox handleChange={handleChange}/>
                <ProfitBox 
                classes={classes} 
                title='Валовая прибыль:' 
                value='5 000 432 р' 
                />
                <ProfitBox 
                classes={classes} 
                title='Валовая прибыль:' 
                value='5 000 432 р' 
                />
            </Box>
            <Box component="div" className={classes.chartsContainer}>
                <ChartBox classes={classes} title='Заказы' />
                <ChartBox classes={classes} title='Продажи' />
                <BestCommodity classes={classes} commodityName='Название товара' />
            </Box>
            <Box>
                <TabPages />
            </Box>
            <Box>
                
            </Box>
        </Box>
    )
}