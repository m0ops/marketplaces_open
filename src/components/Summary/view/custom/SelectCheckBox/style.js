import { Box, Button, Container, InputBase, Link, Typography } from "@material-ui/core";
import { makeStyles, withStyles } from "@material-ui/styles";
import arrowIcon from '../../../../../static/assets/img/arrow.png';

export const useStyles = makeStyles((theme) => ({
  fz20: {
    fontWeight: '600',
    fontSize: '20px',
    lineHeight: '24px',
    color: '#2F2F2F',
  },
  fz18: {
    fontWeight: '600',
    fontSize: '18px',
    lineHeight: '22px',
    color: '#2F2F2F',
  },
  fz18: {
    fontWeight: 'normal',
    fontSize: '16px',
    lineHeight: '19px',
    color: '#2F2F2F',
  },
  selectContainer: {
    position: 'relative',
    width: '309px', 
    marginBottom: '30px',
    flexGrow: '1',
  },
  select: {
    display: 'flex',
    justifyContent: 'left',
    alignItems: 'center',
    width: '309px',
    height: '58px',
    background: '#FFF7F4',
    border: '1px solid #FFE8E0',
    borderRadius: '16px',
    padding: '19px 0',
    fontSize: '16px', 
    cursor: 'pointer',
    '&:hover': {
      border: '1px solid #FFCBBA',
    },
    '&:active': {
        border: '1px solid #F98A67',
    },
    '&::before': {
      position: 'relative',
      display: 'inline-block',
      boxSizing: 'border-box',
      width: '24px',
      height: '24px',
      content: '""',
      left: '88%',
      backgroundImage: `url(${arrowIcon})`,
      backgroundSize: '8px 14px',
      backgroundPosition: 'center',
      backgroundRepeat: 'no-repeat',
      cursor: 'pointer',
    },
  },
  rotate: {
    '&::before': {
      transform: 'rotate(90deg)',
    },
  },
  selectMenu: {
    position: 'absolute',
    width: '309px',
    opacity: '0',
    background: '#FFF7F4',
    border: '1px solid #FFCBBA',
    borderRadius: '0 0 16px 16px',
    padding: '10px',
    display: 'none',
    zIndex: '100',
    cursor: 'pointer',
  },
  selectSubMenu: {
    width: '100%',
    background: '#FFF7F4',
    padding: '10px',
    cursor: 'pointer',
    display: 'none',
    opacity: '0',
    zIndex: '100',
    '&:last-child': {
      borderBottom: 'none',
      borderRadius: '0 0 16px 16px',
    },
  },
  selectItem: {
    listStyle: 'none',
    padding: '7px 10px 7px 22px',
    cursor: 'pointer',
    borderBottom: '1px solid #FFE8E0',
    borderTop: '1px solid #FFE8E0',
    '&:hover': {
      background: '#fff5ee',
    },
    '&:last-child': {
      borderBottom: 'none',
      borderRadius: '0 0 16px 16px',
    },
  },
  selectSubItem: {
    listStyle: 'none',
    padding: '7px 10px 7px 60px',
    cursor: 'pointer',
    borderBottom: '1px solid #FFE8E0',
    '&:hover': {
      background: '#fff5ee',
    },
    '&:last-child': {
      borderBottom: 'none',
      borderRadius: '0 0 16px 16px',
    },
  },
}))
