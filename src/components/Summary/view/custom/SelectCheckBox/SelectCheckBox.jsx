import React from "react";
import clsx from "clsx";
import { useStyles } from "./style.js";
import { CustomCheckBox } from "../CheckBox/CheckBox";

const handleSelectClick = (isClick) => { 
    const selectButton = document.querySelector('#select-button');
    const selectMenu = document.querySelector('#select-menu');
    const selectSubMenu = document.querySelectorAll('#select-submenu');

    if (isClick) {
        selectMenu.style.opacity = '1';
        selectMenu.style.display = 'block';
        for (let i=0; i<selectSubMenu.length; i++) {
            selectSubMenu[i].style.opacity = '1';
            selectSubMenu[i].style.display = 'block';
        } 
        selectButton.style.borderRadius = '16px 16px 0px 0px';
    } else {
        selectMenu.style.opacity = '0';
        selectMenu.style.display = '';
        for (let i=0; i<selectSubMenu.length; i++) {
            selectSubMenu[i].style.opacity = '0';
            selectSubMenu[i].style.display = '';
        } 
        selectButton.style.borderRadius = '16px';
    }
    
}

// const handleSelectItemClick = (event, setName) => { 
//     //const selectItem = document.querySelector('.makeStyles-selectMenu-7');
//     //const selectMenu = document.querySelector('.makeStyles-selectMenu-37');  
//     const selectItem = document.querySelector('.makeStyles-selectItem-39');
//     console.log(event.target);
//     //selectMenu.style.opacity = '0';
//     //selectMenu.style.display = '';
//     //selectButton.style.borderRadius = '16px';
//     //setName(event.target.textContent);
// }

const getSelectSubItems = (classes, count) => {
    const items = [];
    for(let i=0; i<count; i++)
        items.push(
            <li 
            className={classes.selectSubItem} 
            key={i}
            >
                <CustomCheckBox label='Аккаунт'/>
            </li>
        )
    return items;
}


export const SelectCheckBox = ({ handleChange }) => {
    const classes = useStyles();
    const markets = [
        {
            name: 'OZON',
            accountsCount: 3,
        },
        {
            name: 'WildBerrys',
            accountsCount: 2,
        },
        {
            name: 'Amazon',
            accountsCount: 2,
        },
    ];
    const [isClick, setClick] = React.useState(false);
    const [nameMarket, setName] = React.useState('Выберите маркетплейс');
    const [checkedMarketList] = React.useState([]);

    const handleSelectItemClick = (event, value) => {
        if (event.target.checked)
        {
            checkedMarketList.push(value);
            handleChange(String(checkedMarketList));
        } 
        else
        {
            let index_for_del = checkedMarketList.findIndex(item => item === value);
            checkedMarketList.splice(index_for_del, 1);
            handleChange(String(checkedMarketList));
        }
            
    }

    React.useEffect(() => {
        handleSelectClick(isClick)
    }, [isClick]);

    return (
        <div className={classes.selectContainer}>
            <button 
            id='select-button'
            className={ clsx(classes.select, isClick && classes.rotate) } 
            onClick={() => { setClick(!isClick) }}>
                {String(checkedMarketList)}
            </button>
            <ul className={classes.selectMenu} id='select-menu'>
                {markets.map((market, i) => (
                    <div key={i} >
                        <li                       
                        className={classes.selectItem} 
                        >
                            <CustomCheckBox label={market.name} handleSelectItemClick={handleSelectItemClick}/>                       
                        </li> 
                        <ul className={classes.selectSubMenu} id='select-submenu'>
                            {getSelectSubItems(classes, market.accountsCount)}
                        </ul>
                    </div> 
                ))}
            </ul>
        </div>
        
    );
}