import { Box, Button, Container, InputBase, Link, Typography } from "@material-ui/core";
import { makeStyles, withStyles } from "@material-ui/styles";
import checkboxIcon from '../../../../../static/assets/img/check-box-icon.png';

export const useStyles = makeStyles((theme) => ({
  root: {
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  icon: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '25px',
    height: '25px',
    borderRadius: '50%',
    border: '1px solid #F98A67',
    'input:hover ~ &': {
      backgroundColor: '#F98A67',
    }
  },
  checkedIcon: {
    background: 'linear-gradient(162.42deg, #FFB29A 0.8%, #F98A67 98.37%)',
    '&:before': {
      display: 'block',
      width: '12px',
      height: '9px',
      backgroundImage: `url(${checkboxIcon})`,
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      backgroundRepeat: 'no-repeat',
      content: '""',
    },
    'input:hover ~ &': {
      boxShadow: '0px 6px 14px rgba(251, 129, 91, 0.63)',
    },
  }
}))
