import React from "react";
import clsx from "clsx";
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { useStyles } from "./style.js";

const CheckBox = ({classes, handleClick, label, ...props}) => {
    return(
        <Checkbox
        className={classes.root}
        disableRipple
        checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
        icon={<span className={classes.icon} />}
        onChange={(event) => { handleClick(event, label) }}
        {...props}
        />
    )
}

export const CustomCheckBox = ({label, handleSelectItemClick, ...props}) => {
    const classes = useStyles();

    return(
        <FormControlLabel
        style={{width: '100%'}}
        control={<CheckBox classes={classes} handleClick={handleSelectItemClick} label={label} />}
        label={label}
        {...props}
        />
    )
}