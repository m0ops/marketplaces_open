import React from "react";
import { Line } from 'react-chartjs-2'

const dataLineChart = (canvas) => {
    let ctx = canvas.getContext("2d")
    let gradient = ctx.createLinearGradient(0, 0, 0, 200);
    gradient.addColorStop(0, 'rgba(255, 119, 43, 1)');
    gradient.addColorStop(1, 'rgba(255, 119, 43, 0)');
    return {
        labels: ['01.02', '01.02', '01.02', '01.02', '01.02', '01.02', '01.02', '01.02'],
        datasets: [{
            label: '# of Votes',
            data: [1, 12, 19, 10, 9, 15, 13, 1],
            backgroundColor: gradient,
            borderColor: [
                'rgba(255, 99, 132, 1)',
            ],
            borderWidth: 1,
            fill: true,
            cubicInterpolationMode: 'monotone',
        },
    ]
    }
}

export const LineChart = () => {

    return (
            <Line 
            id='line-chart-1'
            data={dataLineChart}
            options={{
                scales: {
                    y: {
                        min: 0,              
                        max: 30,
                        ticks: {
                            beginAtZero: true
                        },
                        grid: {
                            color: '#FFF7F4',
                            borderColor: '#FFE8E0',
                        }
                    },
                    x: {
                        grid: {
                            display: false,
                            borderColor: '#FFE8E0',
                        }
                    }
                },
                plugins: {
                    legend: {
                    display: false
                    }
                }}}
        />
    )
}