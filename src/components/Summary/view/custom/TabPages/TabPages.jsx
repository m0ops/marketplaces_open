import React from 'react';
import { StyledTabs, StyledTab, useStyles } from './style.js'
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import { Box, Typography } from '@material-ui/core';
import { CustomTable } from '../Table/CustomTable.jsx';
import { mergeClasses } from '@material-ui/styles';

const TabPanel = (props) => {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`wrapped-tabpanel-${index}`}
        aria-labelledby={`wrapped-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box style={{padding: '20px'}}>
            {children}
          </Box>
        )}
      </div>
    );
}
  
TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

const a11yProps = (index) => {
return {
    id: `wrapped-tab-${index}`,
    'aria-controls': `wrapped-tabpanel-${index}`,
};
}

export const TabPages = () => {
    const classes = useStyles();
    const [value, setValue] = React.useState('one');

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <div className={classes.root}>
            <StyledTabs value={value} onChange={handleChange} >     
                <StyledTab value='one' disableRipple label="Товары в пути"  {...a11yProps('one')} />  
                <StyledTab value='two' disableRipple label="Остатки на складе" {...a11yProps('two')} />
                <StyledTab value='three' disableRipple label="Предложения по новым товарам" {...a11yProps('three')} />
            </StyledTabs>

            
            <TabPanel value={value} index='one'>
                <CustomTable />
            </TabPanel>
            <TabPanel value={value} index="two">
                Item Two
            </TabPanel>
            <TabPanel value={value} index="three">
                Item Three
            </TabPanel>
        </div>
    )
}