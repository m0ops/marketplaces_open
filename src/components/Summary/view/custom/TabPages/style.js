import { makeStyles, withStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

export const useStyles = makeStyles((theme) => ({
    root: {
        background: '#fff',
        boxShadow: '51px 6px 51px rgba(77, 18, 0, 0.13)',
    },
    appBar: {
        boxShadow: 'none',
    },
}))

export const StyledTabs = withStyles({
    root: {
      background: '#F98A67',
      borderRadius: '0 16px 0 0',
      width: '720px',
    },
    indicator: {
      display: 'flex',
      justifyContent: 'center',
      backgroundColor: 'transparent',
    },
  })(Tabs)
  
 export const StyledTab = withStyles((theme) => ({
    root: {
      minWidth: 'max-content',
      textTransform: 'none',
      color: '#fff',
      fontWeight: 'normal',
      fontSize: '20px',
      lineHeight: '24px',
      transition: '200ms',
      opacity: 1,
      '&.Mui-selected': {
        color: '#F98A67',
        background: '#fff',

      },
    //   '&.Mui-selected::after': {
    //     content: '""',
    //     position: 'absolute',
    //     display: 'block',
    //     width: '100%',
    //     height: '20px',
    //     backgroundColor: '#fff',
    //     borderTopRightRadius: '20px',
    //     top: '0'
    //   },
    //   '&.Mui-selected::before': {
    //     content: '""',
    //     position: 'absolute',
    //     display: 'block',
    //     width: '100%',
    //     height: '20px',

    //     backgroundColor: '#F98A67',
    //     top: '0'
    //   }
    },
  }))(Tab)
  

