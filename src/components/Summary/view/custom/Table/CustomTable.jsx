import React from 'react';
import { useStyles, StyledTableCell } from './style.js'
import PropTypes from 'prop-types';
import { Box, Typography } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
  
const createData = (data) => {
    return { data };
}

const rows = [
    createData(<span>&nbsp;</span>),
    createData(<span>&nbsp;</span>),
    createData(<span>&nbsp;</span>),
];

export const CustomTable = (props) => {
    const classes = useStyles();

    return (
        <TableContainer component={Paper} className={classes.tableContainer}>
            <Table className={classes.table} size="small" aria-label="a dense table">
                <TableHead>
                    <TableRow>
                        <StyledTableCell>Дата</StyledTableCell>
                        <StyledTableCell>Дата</StyledTableCell>
                        <StyledTableCell>Дата</StyledTableCell>
                        <StyledTableCell>Дата</StyledTableCell>
                        <StyledTableCell>Дата</StyledTableCell>
                        <StyledTableCell>Дата</StyledTableCell>
                        <StyledTableCell>Дата</StyledTableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                {rows.map((row, index) => (
                    <TableRow key={index}>
                        <StyledTableCell>{row.data}</StyledTableCell>
                        <StyledTableCell>{row.data}</StyledTableCell>
                        <StyledTableCell>{row.data}</StyledTableCell>
                        <StyledTableCell>{row.data}</StyledTableCell>
                        <StyledTableCell>{row.data}</StyledTableCell>
                        <StyledTableCell>{row.data}</StyledTableCell>
                        <StyledTableCell>{row.data}</StyledTableCell>
                    </TableRow>
                ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}