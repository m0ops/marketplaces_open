import { Box, Button, Container, InputBase, Link, Typography } from "@material-ui/core";
import { makeStyles, withStyles } from "@material-ui/styles";
import TableCell from '@material-ui/core/TableCell';
import sortIcon from '../../../../../static/assets/img/sort-icon.png'

export const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  tableContainer: {
    border: '1px solid #FFE8E0',
    borderRadius: '16px',
    boxShadow: 'none',
  },
  table: {
    minWidth: 650,  
  },
}))

export const StyledTableCell = withStyles((theme) => ({
  root: {
    borderRight: '1px solid #FFE8E0',
    fontSize: 14,
    '&:last-child': {
      borderRight: 'none',
    },
  },
  head: {
    position: 'relative',
    backgroundColor: '#FFF7F4',
    '&::before': {
      position: 'absolute',
      display: 'inline-block',
      boxSizing: 'border-box',
      content: '""',
      width: '1px',
      height: '100%',
      top: '0',
      right: '0',
      borderRight: '1px solid #FFE8E0',
    },
    '&:last-child::before': {
      borderRight: 'none',
    },
    '&::after': {
      position: 'absolute',
      display: 'inline-block',
      boxSizing: 'border-box',
      width: '18px',
      height: '18px',
      content: '""',
      right: '10px',
      top: '8px',
      backgroundImage: `url(${sortIcon})`,
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      backgroundRepeat: 'no-repeat',
      cursor: 'pointer',
    }
  },
  body: {
    fontSize: 14,
    borderBottom: '1px solid #FFE8E0',
  },
}))(TableCell);
