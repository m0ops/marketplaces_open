import React from "react";
import { Redirect, Route } from "react-router-dom";
import { Settings } from "../Settings";
import { Summary } from "../Summary/view/Summary";

export const Routes = ({}) => {
  const isAuth = localStorage.getItem("token") ? true : false;

  const MainRedirect = () => {
    return <Redirect to="/" />;
  };

  const routes = [
    { path: "/settings", Component: Settings },
    { path: "/summary", Component: Summary },
    // { path: "/settings", Component: isAuth ? Settings : MainRedirect },
    // { path: "/summary", Component: isAuth ? Summary : MainRedirect },
    // { path: "/summary", Component: Settings },
    // { path: "/analitics", Component: Settings },
    // { path: "/reporting", Component: Settings },
    // { path: "/my-goods", Component: Settings },
    // { path: "/storages", Component: Settings },
    // { path: "/order-processing", Component: Settings },
    // { path: "/chat", Component: Settings },
    // { path: "/settings", Component: Settings },
  ];

  return (
    <>
      {routes.map(({ path, Component }) => (
        <Route key={path} exact path={path}>
          <Component />
        </Route>
      ))}
    </>
  );
};
