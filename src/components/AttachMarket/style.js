import { Box, Button, Container, InputBase, Link, Typography } from "@material-ui/core";
import { makeStyles, withStyles } from "@material-ui/styles";
import arrowIcon from '../../static/assets/img/arrow.png';
import howAddIcon from '../../static/assets/img/how-add-icon.png';
import addIcon from '../../static/assets/img/add-icon.png';

export const useStyles = makeStyles((theme) => ({
    root: {
      position: 'relative',
      backgroundColor: '#fff',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      boxSizing: 'border-box',
      fontFamily: 'Inter, sans-serif',
      marginBottom: '40px',
    },
    attachBox: {
      position: 'relative',
      width: '512px',
      minHeight: '465px',
      background: '#FFFFFF',
      border: '1px solid rgba(0, 0, 0, 0.123)',
      borderRadius: '29px',
      boxShadow: '0px 45px 111px -33px rgba(1, 1, 1, 0.1)',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      padding: '30px 30px 0 30px',
      margin: '0 20px'
    },
    formControl: {
      position: 'relative',
      display: 'flex',
      justifyContent: 'right',
      alignItems: 'center',
      background: '#FFF7F4',
      border: '1px solid #FFE8E0',
      boxSizing: 'border-box',
      borderRadius: '16px',
      width: '100%',
      height: '57px',
    },
    inputBox: {
      width: '100%',
      marginBottom: '15px',
    },
    select: {
      display: 'flex',
      justifyContent: 'left',
      alignItems: 'center',
      width: '100%',
      height: '58px',
      background: '#FFF7F4',
      border: '1px solid #FFE8E0',
      borderRadius: '16px',
      padding: '19px 18px',
      fontSize: '16px', 
      cursor: 'pointer',
      '&:hover': {
        border: '1px solid #FFCBBA',
      },
      '&:active': {
          border: '1px solid #F98A67',
      },
      '&::before': {
        position: 'relative',
        display: 'inline-block',
        boxSizing: 'border-box',
        width: '24px',
        height: '24px',
        content: '""',
        left: '95%',
        backgroundImage: `url(${arrowIcon})`,
        backgroundSize: '8px 14px',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        cursor: 'pointer',
      },
    },
    rotate: {
      '&::before': {
        transform: 'rotate(90deg)',
      },
    },
    selectMenu: {
      position: 'absolute',
      width: 'calc(100% - 60px)',
      opacity: '0',
      background: '#FFF7F4',
      border: '1px solid #FFCBBA',
      borderRadius: '0 0 16px 16px',
      padding: '10px',
      display: 'none',
      zIndex: '100',
      cursor: 'pointer',
    },
    selectItem: {
      listStyle: 'none',
      padding: '19px 18px',
      cursor: 'pointer',
      borderBottom: '1px solid #FFCBBA',
      '&:hover': {
        background: '#fff5ee',
      },
      '&:last-child': {
        borderBottom: 'none',
        borderRadius: '0 0 16px 16px',
      },
    },
    spaceCenterRowItem: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      width: '100%',
    },
    centerCenterColumnItem: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      minHeight: '100vh',
    },
    helpButton: {
      boxSizing: 'border-box',
      width: '46px',
      height: '46px',
      background: '#F98A67',
      border: 'none',
      borderRadius: '16px',
      backgroundImage: `url(${howAddIcon})`,
      backgroundSize: '14px 17px',
      backgroundPosition: 'center',
      backgroundRepeat: 'no-repeat',
      color: '#FFFFFF',
      cursor: 'pointer',
      marginLeft: '20px', 
      '&:hover': {
          backgroundColor: '#FF9F81',
      },
      '&:active': {
          backgroundColor: '#F4805B',
      },
    },
    text20: {
      fontWeight: '600',
      fontSize: '20px',
      lineHeight: '24px',
      color: '#2F2F2F',
    },
    text30: {
      fontWeight: '600',
      fontSize: '30px',
      lineHeight: '36px',
      color: '#2F2F2F',
    },
    splitLine: {
      background: '#FFE8E0',
      width: '100%',
      height: '1px',
      border: 'none',
    },
    addAccount: {
      display: 'flex',
      justifyContent: 'left',
      alignItems: 'center',
      width: '100%',
      height: '58px',
      background: '#FFF7F4',
      border: '1px solid #FFE8E0',
      borderRadius: '16px',
      padding: '19px 18px',
      fontSize: '16px', 
      '&:hover': {
        border: '1px solid #FFCBBA',
      },
      '&:active': {
          border: '1px solid #F98A67',
      },
      '&::before': {
        position: 'relative',
        display: 'inline-block',
        boxSizing: 'border-box',
        width: '25px',
        height: '25px',
        content: '""',
        left: '95%',
        backgroundImage: `url(${addIcon})`,
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        cursor: 'pointer',
      },
    },
  }));

  export const Label = withStyles({
    root: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        fontWeight: 'normal',
        fontSize: '16px',
        lineHeight: '19px',
        color: '#2F2F2F',
    },
  })(Typography);

  export const EmptyTextError = withStyles({
    root: {
        display: 'inline-block',
        fontWeight: 'normal',
        fontSize: '14px',
        lineHeight: '17px',
        color: '#FF2F2F',
    },
  })(Typography);

  export const LinkLabel = withStyles({
    root: {
        fontWeight: '600',
        fontSize: '16px',
        lineHeight: '19px',
        color: '#F98A67',
        transition: '300ms',
        textDecoration: 'none',
        cursor: 'pointer',
        '&:hover': {
            color: '#FF9F81',
            textDecoration: 'underline',
        },
        '&:active': {
            color: '#F4805B',
            textDecoration: 'underline',
        },
    },
  })(Link);

  export const TitleBox = withStyles({
    root: {
        fontWeight: '600',
        fontSize: '30px',
        lineHeight: '36px',
        color: '#2F2F2F',
    },
  })(Typography);

  export const SubmitButton = withStyles({
    root: {
      width: '16%',
      height: '55px',
      padding: '18px 37px',
      background: '#F98A67',
      border: 'none',
      borderRadius: '16px',
      fontWeight: 'normal',
      fontSize: '16px',
      lineHeight: '19px',
      textAlign: 'center',
      color: '#FFFFFF',
      marginBottom: '10px',
      textTransform: 'none',
      '&:hover': {
          backgroundColor: '#FF9F81',
      },
      '&:active': {
          backgroundColor: '#F4805B',
      },
    }
})(Button);

export const AddAccountButton = withStyles({
  root: {
    margin: '0',
    paddingRight: '15px',
    width: '50%',
    height: '58px',
    right: '25px',
    background: 'transparent',
    border: 'none',
    borderRadius: '16px',
    fontSize: '14px', 
    color: '#F98A67',
    textTransform: 'none',
    alignSelf: 'flex-start',
    textAlign: 'left',
    '&:hover': {
      color: '#FFCBBA',
      backgroundColor: 'transparent',
    },
    '&:active': {
      color: '#F98A67',
    },
    '&:hover:before': {
      opacity: '0.5',
    },
    '&:active:before': {
      opacity: '0.9',
    },
    '&::before': {
      position: 'relative',
      display: 'inline-block',
      boxSizing: 'border-box',
      width: '25px',
      height: '25px',
      content: '""',
      left: '15px',
      backgroundImage: `url(${addIcon})`,
      backgroundPosition: 'center',
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'contain',
      cursor: 'pointer',
    },
  }
})(Button);

  export const TextInput = withStyles({
    root: {
      width: '100%',
      height: '58px',
      background: '#FFF7F4',
      border: '1px solid #FFE8E0',
      borderRadius: '16px',
      padding: '19px 18px',
      fontSize: '26px', 
      textAlign: 'center',
      '& input': {
        textAlign: "center",
      },
      '&:hover': {
          border: '1px solid #FFCBBA',
      },
      '&.Mui-focused': {
          border: '1px solid #F98A67',
      },
      '&.Mui-error' : {
        backgroundColor: '#FFDBDB',
        borderColor: '#FF2F2F',
      }
    }
  })(InputBase);

  export const ArrowIcon = withStyles({
    root: {
        position: 'relative',
        width: '24px',
        height: '24px',
        backgroundImage: `url(${arrowIcon})`,
        backgroundSize: '8px 14px',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        cursor: 'pointer',
        transition: '300ms',
    },
  })(Box);
