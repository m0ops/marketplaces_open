import React from "react";
import clsx from "clsx";
import { useStyles, LinkLabel, Label, TextInput, SubmitButton, AddAccountButton } from "./style";
import { Box } from "@material-ui/core";
import { useInput } from "../Auth/Form/validForm";


const handleHowAddAccountClick = () => {
    alert('hpw add account');
}

const handleAddAccountClick = () => {
    alert('add account');
}

const handleSelectClick = (isClick, index) => { 
    const selectButton = document.querySelectorAll('.makeStyles-select-5')[index];
    const selectMenu = document.querySelectorAll('.makeStyles-selectMenu-7')[index];
    if (isClick) {
        selectMenu.style.opacity = '1';
        selectMenu.style.display = 'block';
        selectButton.style.borderRadius = '16px 16px 0px 0px';
    } else {
        selectMenu.style.opacity = '0';
        selectMenu.style.display = '';
        selectButton.style.borderRadius = '16px';
    }
    
}

const handleSelectItemClick = (event, setName, index) => { 
    //const selectItem = document.querySelector('.makeStyles-selectMenu-7');
    const selectMenu = document.querySelectorAll('.makeStyles-selectMenu-7')[index];  
    const selectButton = document.querySelectorAll('.makeStyles-select-5')[index];
    selectMenu.style.opacity = '0';
    selectMenu.style.display = '';
    selectButton.style.borderRadius = '16px';
    setName(event.target.textContent);
}

const AddAccountForm = ({classes, inputAccountName, inputAPI}) => {

    return(
        <div>
            <Box className={classes.inputBox}>
                <Label component="div" style={{ marginBottom: "8px" }}>
                    Название аккаунта
                    { inputAccountName.getErrorEmpty() }
                </Label>
                <TextInput 
                    onBlur={(e) => { inputAccountName.onBlur(e) }} 
                    onChange={(e) => { inputAccountName.onChange(e) }} 
                    error={ inputAccountName.isEmpty && inputAccountName.isAfterClick }
                />
            </Box>
            <Box className={classes.inputBox}>
                <Label component="div" style={{ marginBottom: "8px" }}>
                    API-ключ от аккаунта
                    { inputAPI.getErrorEmpty() }
                </Label>
                <TextInput 
                    onBlur={(e) => { inputAPI.onBlur(e) }} 
                    onChange={(e) => { inputAPI.onChange(e) }} 
                    error={ inputAPI.isEmpty && inputAPI.isAfterClick }
                />
            </Box>
        </div>
    )
}

const CustomSelect = ({ classes, handleChange, marketPlace, index, ...props }) => {
    const markets = ['OZON', 'WildBerrys', 'Amazon']
    const [isClick, setClick] = React.useState(false);
    const [nameMarket, setName] = React.useState('Выберите маркетплейс');

    React.useEffect(() => {
        handleSelectClick(isClick, index)
    }, [isClick])

    return (
        <div style={{width: '100%', marginBottom: '50px'}}>
            <button 
            className={ clsx(classes.select, isClick && classes.rotate) } 
            onClick={() => { setClick(!isClick) }}>
                {nameMarket}
            </button>
            <ul className={classes.selectMenu}>
                {markets.map((market, i) => (
                    <li 
                    key={i} 
                    className={classes.selectItem} 
                    onClick={(event) => { 
                        handleSelectItemClick(event, setName, index), 
                        setClick(false) 
                    }}
                    >
                    {market}
                    </li> 
                ))}
            </ul>
        </div>
        
    );
}

const AttachMarketForm = ({ classes, handleCombineInvalid, isClick, index }) => {
    const [marketPlace, setMarket] = React.useState('')
    const inputAccountName = useInput('', {isEmpty: true});
    const inputAPI = useInput('', {isEmpty: true});

    const handleChange = (event) => {
        setMarket(event.target.value);
    };

    handleCombineInvalid({inputAccountName, inputAPI});

    React.useEffect(() => {
        if (isClick) {
            inputAccountName.onAfterClick(true);
            inputAPI.onAfterClick(true);
        }  
    }, [isClick]);

    return (     
        <div className={classes.attachBox}>
            <CustomSelect classes={classes} handleChange={handleChange} value={marketPlace} index={index} />
            <div className={classes.spaceCenterRowItem} style={{marginBottom: '35px'}}>
                <p className={classes.text20}>Добавьте ваши аккаунты</p>
                <div className={classes.helpButton} onClick={handleHowAddAccountClick}></div>
                <LinkLabel href='#' onClick={handleHowAddAccountClick}><p>Как добавить<br /> аккаунт?</p></LinkLabel> 
            </div>
            <AddAccountForm classes={classes} inputAccountName={inputAccountName} inputAPI={inputAPI} />

            <hr className={classes.splitLine}/>
            <AddAccountButton 
            disableRipple 
            variant="outlined" 
            style={{marginBottom: '20px'}}
            onClick={handleAddAccountClick}
            >
                Добавить аккаунт
            </AddAccountButton>
        </div>
    )
}

export const AttachMarket = () => {
    const classes = useStyles();
    const [isClick, setClick] = React.useState(false); 

    let combineInvalid = false;
    const handleCombineInvalid = (inputs) => {
        if (isClick){
            for (const input in inputs) 
                combineInvalid = combineInvalid || inputs[input].isEmpty;
        }
        
    }
    const isInvalid = () => {
        combineInvalid ? alert('error') : alert('ok');
    }

    React.useEffect(() => {
        setClick(false);
        if (isClick)
            isInvalid();
    }, [isClick]);

    return (
        <div className={classes.centerCenterColumnItem}>
            <p className={classes.text30} style={{marginBottom: '50px'}}>Привязать маркетплейсы</p>
            <div className={classes.root}>
                {[1,1,1].map((item, index) => (
                    <AttachMarketForm 
                    key={index} 
                    index={index} 
                    classes={classes} 
                    handleCombineInvalid={handleCombineInvalid} 
                    isClick={isClick} />
                ))}
            </div>     
            <SubmitButton
            disableRipple
            variant="outlined"
            onClick={() => { 
                setClick(true);
            }}
            >
                Привязать маркетплейс
            </SubmitButton>
        </div>
        
    )
}