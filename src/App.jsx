import React, { useEffect } from "react";
import { Switch } from "react-router-dom";
// import { BrowserRouter as Router } from "react-router-dom";
import { HashRouter as Router } from "react-router-dom";
import { theme } from "./themes/base";
import { ThemeProvider } from "@material-ui/styles";
import { Layout } from "./components/Layout/Layout";
import "./app.scss";
import { SnackbarProvider } from "notistack";
import { userService } from "./services/user.service";
import { useDispatch } from "react-redux";
import { userActions } from "./store/User";

const App = () => {
  const dispatch = useDispatch();
  const setUserData = async () => {
    try {
      const res = await userService.getUserData();
      const { data } = res;
      dispatch(userActions.setUserData(data));
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    if (localStorage.getItem("token")) {
      console.log(123);
      setUserData();
    }
  }, [localStorage.getItem("token")]);

  return (
    <Router>
      <Switch>
        <ThemeProvider theme={theme}>
          <SnackbarProvider
            maxSnack={3}
            anchorOrigin={{
              vertical: "top",
              horizontal: "right",
            }}
          >
            <Layout />
          </SnackbarProvider>
        </ThemeProvider>
      </Switch>
    </Router>
  );
};

export default App;
